FROM fluendo/docker-buildbot-xenial-x86

MAINTAINER fluendo

# Install required packages
RUN python3 -m pip install requests tinys3 six junit_xml

RUN apt-get update && apt-get upgrade -y

# Missing bootstrap packages
RUN apt-get install -y autotools-dev automake libtool cmake bison flex texinfo doxygen \
                       yasm pkg-config gtk-doc-tools \
                       libxv-dev libpulse-dev python-dev libxi-dev \
                       x11proto-record-dev libxrender-dev libgl1-mesa-dev libxfixes-dev libxdamage-dev \
                       libxcomposite-dev libasound2-dev libxml-simple-perl debhelper \
                       devscripts transfig gperf libdbus-glib-1-dev libgtk2.0-dev \
                       glib-networking-common subversion wine curl rpm vim

# Set default encoding to UTF-8
RUN apt-get install -y locales
RUN locale-gen en_US en_US.UTF-8
RUN update-locale en_US.UTF-8
ENV LC_ALL="en_US.UTF-8"

# Fix the uname call outputing 64 bits arch on 32 bits docker image.
ENTRYPOINT ["/usr/bin/linux32", "--"]

COPY ./run.sh .
COPY ./apt-get /usr/local/sbin/apt-get
CMD ["./run.sh"]
